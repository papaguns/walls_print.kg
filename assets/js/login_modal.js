
$('#send_login_form').click(function () {
        var user_name = $('#user_name').val();
        var password = $('#password').val();
        var url =$('#login-modal-form').attr('action');
        sendLoginForm(user_name, password, url);
    });

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function sendLoginForm(user_name, password, url) {
    if (user_name || password) {
        $.ajax({
            method: 'post',
            url: url,
            data: {
                'user_name': user_name,
                'password': password,
            },
            headers: {
                'X-CSRFToken': getCookie('csrftoken')
            },
            success: function (data) {
                location.reload()
            },
            error: function (xhr, status) {
                alert("Пользователь не найден!");
            },
        })
    }
    }