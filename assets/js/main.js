$('.phone_mask').mask("(+999) 999-99-99-99");

$('.modal-success .close').on('click', function () {
    $('.modal-success').removeClass("show");
    $('body').removeClass('overflow-hidden');
})

$(document).ready(function () {
    $(".link-anchor").on("click", function (event) {
        event.preventDefault();
        $(".menu-hamburger").removeClass('active-mobil-menu');
        $(".list-link__wrapper").removeClass("show");
        $("body").removeClass("overflow-hidden");
        var id = $(this).attr('href'),
                top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});
