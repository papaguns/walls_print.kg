import re

from django.core.validators import RegexValidator


# phone_regex = RegexValidator(
#     regex=r'^\+996\d{9}$',
#     message='Format must be +996123456'
# )


def clean_phone(phone):
    return '+' + re.sub(r"\D", "", str(phone))
