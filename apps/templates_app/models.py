from django.db import models

# from core.helpers import phone_regex


class Order(models.Model):
    class Meta:
        verbose_name = '1. Заказ'
        verbose_name_plural = '1. Заказы'

    MATERIAL_CHOICES = (
        ('Кожа', 'Кожа'),
        ('Пластик', 'Пластик'),
        ('Ткань', 'Ткань'),
        ('Холст', 'Холст'),
        ('Кирпич', 'Кирпич'),
        ('Обои', 'Обои'),
        ('Стекло', 'Стекло'),
        ('Дерево', 'Дерево'),
        ('Металл', 'Металл'),
        ('Паркет', 'Паркет'),
        ('Ламинат', 'Ламинат'),
        ('Штукатурка', 'Штукатурка'),
    )

    material = models.CharField(
        verbose_name='Материал',
        max_length=25,
        choices=MATERIAL_CHOICES,
        null=True,
    )

    first_name = models.CharField(
        max_length=255,
        verbose_name='Имя',
    )
    phone_number = models.CharField(
        max_length=19,
        verbose_name='Телефон',
        # validators=[phone_regex]
    )
    created_at = models.DateField(
        auto_now_add=True,
        verbose_name='Дата отправки',
        null=True,
    )
    is_read = models.BooleanField(
        default=False,
        verbose_name='Прочитано?',
    )

    height = models.IntegerField(
        verbose_name='Высота',
        null=True,
        blank=True
    )

    width = models.IntegerField(
        verbose_name='Ширина',
        null=True,
        blank=True
    )

    price = models.IntegerField(
        verbose_name='Цена',
        null=True,
        blank=True
    )
    image = models.FileField(
        null=True, blank=True,
        upload_to='feedback_images',
        verbose_name='Картинка'
    )

    def __str__(self):
        return self.first_name


class IndividualOrder(models.Model):
    class Meta:
        verbose_name = '2. Индивидуальный Заказ'
        verbose_name_plural = '2. Индивидуальные Заказы'

    first_name = models.CharField(
        max_length=255,
        verbose_name='Имя',
    )
    phone_number = models.CharField(
        max_length=19,
        verbose_name='Телефон',
        # validators=[phone_regex]
    )
    comment = models.TextField(
        verbose_name='Комментарий',
    )
    created_at = models.DateField(
        auto_now_add=True,
        verbose_name='Дата отправки',
        null=True,
    )
    is_read = models.BooleanField(
        default=False,
        verbose_name='Прочитано?',
    )

    def __str__(self):
        return self.first_name


class Gallery(models.Model):
    image = models.FileField(
        null=False,
        blank=False,
        upload_to='gallery',
        verbose_name='Галерея'
    )

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Галерея'


class Counter(models.Model):
    count = models.IntegerField(
        verbose_name='Просмотры',
        default=0
    )

    class Meta:
        verbose_name = 'Просмотр'
        verbose_name_plural = 'Просмотры'

    def __str__(self):
        return self.count


class TelegramChatBot(models.Model):
    class Meta:
        verbose_name = 'Telegram Бот'
        verbose_name_plural = 'Telegram Бот'

    token = models.CharField(
        verbose_name='Telegram Бот Токен',
        max_length=255,
    )
    group_id = models.CharField(
        verbose_name='Group ID',
        max_length=255,
    )

    def __str__(self):
        return f'Telegram чат : {self.token}'
