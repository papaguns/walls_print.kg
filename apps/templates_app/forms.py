from django import forms
from .models import Order, IndividualOrder, Gallery


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ['created_at', 'is_read']
    first_name = forms.CharField()
    phone_number = forms.CharField()
    material = forms.ChoiceField(choices=Order.MATERIAL_CHOICES)
    price = forms.CharField()
    first_name.widget.attrs.update(
        {'class': 'input', 'placeholder': 'Как к Вам обращаться?', 'required': False}
    )
    phone_number.widget.attrs.update(
        {'class': 'input  phone_mask', 'placeholder': 'Номер телефона'}
    )
    material.widget.attrs.update(
        {'class': 'material_choice', 'required': False}
    )
    price.widget.attrs.update(
        {'class': 'input', 'placeholder': 'Цена', 'readonly': 'readonly'}
    )


class IndividualOrderForm(forms.ModelForm):
    class Meta:
        model = IndividualOrder
        exclude = ['created_at', 'is_read']
    first_name = forms.CharField()
    phone_number = forms.CharField()
    comment = forms.CharField(widget=forms.Textarea)
    first_name.widget.attrs.update(
        {'class': 'input', 'placeholder': 'Как к Вам обращаться?',
         'required': False}
    )
    phone_number.widget.attrs.update(
        {'class': 'input  phone_mask', 'placeholder': 'Номер телефона'}
    )
    comment.widget.attrs.update(
        {'class': 'input textarea', 'placeholder': 'Комментарий'}
    )


class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ['image']
