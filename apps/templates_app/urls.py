from django.urls import path

from apps.templates_app.views import (
    IndexView, IndividualOrderView, GalleryView, GalleryListCreateView,
    login_view
)

urlpatterns = [
    # path('gallery/', GalleryView.as_view(), name='gallery_page'),
    path('', IndexView.as_view(), name='index_page'),
    path('individual-order/', IndividualOrderView.as_view(),
         name='individual_order'
         ),
    path('gallery', GalleryListCreateView.as_view(), name='gallery_page'),
    path('login', login_view, name='login')
]
