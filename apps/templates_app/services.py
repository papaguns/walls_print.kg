from django.urls import reverse

import telebot
import logging
from telebot import types
from telebot.apihelper import ApiException


bot = telebot.TeleBot('')

logging.basicConfig(
    filename='telegram_bot.log',
    level=logging.INFO,
    format='%(asctime)s %(message)s',
    datefmt='%m/%d/%Y/ %I:%M:%S %p',
)


def create_bot(token):
    return telebot.TeleBot(token)


def telegramm_bot(request, chat_id, order, bot):
    if hasattr(order, 'price'):
        message = 'Добрый день, поступил заказ \n От: %s,\n Телефон: %s,\n ' \
                  'Материал: %s,\n Цена: %s,\n Размеры: %s X %s\n ' % \
                  (order.first_name,
                   order.phone_number,
                   order.material,
                   order.price,
                   order.height,
                   order.width)
    else:
        message = 'Добрый день, поступил индивидуальный заказ \n От: %s,' \
                  ' \n Телефон: %s,' '\n Комментарий: %s\n ' % \
        (
            order.first_name,
            order.phone_number,
            order.comment
        )
    send_telegram_notification_url(
        message=message,
        group_id=chat_id,
        link=request.build_absolute_uri(
            reverse('admin:templates_app_order_changelist')
        ),
        bot=bot
    )


@bot.message_handler(commands=['url'], )
def send_telegram_notification_url(message, group_id, link, bot):
    markup = types.InlineKeyboardMarkup()
    button_to_site = types.InlineKeyboardButton(text='перейти', url=link)
    markup.add(button_to_site)
    try:
        bot.send_message(
            group_id,
            message,
            reply_markup=markup,
            parse_mode="Markdown"
        )
    except ApiException:
        _send_telegram_notification(chat_id=group_id, message=message)
        logging.info('Telegram API Exception not valid link')


def _send_telegram_notification(chat_id: str, message: str):
    try:
        bot.send_message(chat_id=chat_id, text=message, parse_mode="Markdown")
    except ApiException:
        logging.info("Telegram API Exception: ")
    except Exception as e:
        logging.error(e)
