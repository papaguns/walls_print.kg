from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render
from django.http import JsonResponse

from django.views.generic import TemplateView, CreateView, ListView

from .models import Order, IndividualOrder, Counter, Gallery, TelegramChatBot
from .forms import OrderForm, IndividualOrderForm, GalleryForm

from .services import create_bot, telegramm_bot


class IndexView(CreateView):
    template_name = 'pages/index.html'
    model = Order
    form_class = OrderForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        Counter.objects.get_or_create()
        counter = Counter.objects.first()
        counter.count += 1
        counter.save(update_fields=['count'])
        context['order_form'] = self.form_class
        return context

    def form_invalid(self, form):
        return render(self.request, 'pages/index.html', {'order_form': form})

    def form_valid(self, form):
        order = form.save()
        order.height = self.request.POST.get('height')
        order.width = self.request.POST.get('width')
        order.save()
        telegram = TelegramChatBot.objects.first()
        if telegram:
            group_id = telegram.group_id
            telegramm_bot(
                self.request,
                group_id,
                order,
                create_bot(telegram.token),
            )
        return render(self.request, 'pages/index.html',
                      {'success': 'success', 'order_form': self.form_class}
                      )


class GalleryView(TemplateView):
    template_name = 'pages/gallery.html'


class IndividualOrderView(CreateView):
    template_name = 'pages/individual_order.html'
    model = IndividualOrder
    form_class = IndividualOrderForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['individual_order_form'] = self.form_class
        return context

    def form_invalid(self, form):
        return render(self.request, 'pages/individual_order.html',
                      {'order_form': form}
                      )

    def form_valid(self, form):
        individual_order = form.save()
        individual_order.save()
        telegram = TelegramChatBot.objects.first()
        if telegram:
            group_id = telegram.group_id
            telegramm_bot(
                self.request,
                group_id,
                individual_order,
                create_bot(telegram.token),
            )
        return render(self.request, 'pages/individual_order.html',
                      {
                          'success': 'success',
                          'individual_order_form': self.form_class
                      }
                      )


class GalleryListCreateView(ListView, CreateView):
    model = Gallery
    template_name = 'pages/gallery.html'
    form_class = GalleryForm
    object = Gallery

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['images'] = Gallery.objects.all()
        context['gallery'] = self.form_class
        return context

    def form_valid(self, form):
        gallery = form.save()
        gallery.save()
        return redirect('gallery_page')


def login_view(request):
    username = request.POST.get('user_name')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return JsonResponse({'user': username}, status=200)
    else:
        JsonResponse({'error': 'NOT FOUND!'}, status=404)
