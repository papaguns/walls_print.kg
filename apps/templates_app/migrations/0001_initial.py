# Generated by Django 2.2 on 2021-07-03 17:41

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Counter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField(default=0, verbose_name='Просмотры')),
            ],
            options={
                'verbose_name': 'Просмотр',
                'verbose_name_plural': 'Просмотры',
            },
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.FileField(upload_to='gallery', verbose_name='Галерея')),
            ],
            options={
                'verbose_name': 'Изображение',
                'verbose_name_plural': 'Галерея',
            },
        ),
        migrations.CreateModel(
            name='IndividualOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('phone_number', models.CharField(max_length=17, validators=[django.core.validators.RegexValidator(message='Format must be +996123123123', regex='^\\+996\\d{9}$')], verbose_name='Телефон')),
                ('comment', models.TextField(verbose_name='Комментарий')),
                ('created_at', models.DateField(auto_now_add=True, null=True, verbose_name='Дата отправки')),
                ('is_read', models.BooleanField(default=False, verbose_name='Прочитано?')),
            ],
            options={
                'verbose_name': '2. Индивидуальный Заказ',
                'verbose_name_plural': '2. Индиввидуальные Заказы',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('material', models.CharField(choices=[('Кожа', 'Кожа'), ('Пластик', 'Пластик'), ('Ткань', 'Ткань'), ('Холст', 'Холст'), ('Кирпич', 'Кирпич'), ('Обои', 'Обои'), ('Стекло', 'Стекло'), ('Дерево', 'Дерево'), ('Металл', 'Металл'), ('Паркет', 'Паркет'), ('Ламинат', 'Ламинат'), ('Штукатурка', 'Штукатурка')], max_length=25, null=True, verbose_name='Материал')),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('phone_number', models.CharField(max_length=17, validators=[django.core.validators.RegexValidator(message='Format must be +996123123123', regex='^\\+996\\d{9}$')], verbose_name='Телефон')),
                ('created_at', models.DateField(auto_now_add=True, null=True, verbose_name='Дата отправки')),
                ('is_read', models.BooleanField(default=False, verbose_name='Прочитано?')),
                ('price', models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True, verbose_name='Цена')),
                ('image', models.FileField(blank=True, null=True, upload_to='feedback_images', verbose_name='Картинка')),
            ],
            options={
                'verbose_name': '1. Заказ',
                'verbose_name_plural': '1. Заказы',
            },
        ),
    ]
