from django.contrib import admin

from apps.templates_app.models import (
    Order, IndividualOrder, Counter, Gallery, TelegramChatBot
)

# Register your models here.


@admin.register(Counter)
class CounterAdmin(admin.ModelAdmin):
    class Meta:
        model = Counter


@admin.register(Order)
class CounterAdmin(admin.ModelAdmin):
    class Meta:
        model = Order


@admin.register(IndividualOrder)
class CounterAdmin(admin.ModelAdmin):
    class Meta:
        model = IndividualOrder


@admin.register(Gallery)
class CounterAdmin(admin.ModelAdmin):
    class Meta:
        model = Gallery


@admin.register(TelegramChatBot)
class TelegramAdmin(admin.ModelAdmin):
    class Meta:
        model = TelegramChatBot
